//
//     QuantLET - an event driven framework for large scale real-time analytics
//
//     Copyright (C) 2006 Jorge M. Faleiro Jr.
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Affero General Public License as published
//     by the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Affero General Public License for more details.
//
//     You should have received a copy of the GNU Affero General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
package quantlet.reactive.local;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.Executor;
import java.util.logging.Level;
import java.util.logging.Logger;

import quantlet.reactive.Event;
import quantlet.reactive.GuardException;
import quantlet.reactive.Observer;
import quantlet.reactive.Subscription;

public class SubscriptionLocal<V> implements Subscription<V>{

    private static final Logger logger = Logger.getLogger(SubscriptionLocal.class.getName());

    protected final Map<UUID, Observer<V>> subscribers = new HashMap<>();
    protected final Map<Object, List<UUID>> sources = new HashMap<>();

    private final Executor executor;

    public SubscriptionLocal(final Executor executor) {
        this.executor = executor;

    }

    public void unsubscribe(final Object source) {
        final var indices = this.sources.get(source);
        indices.forEach(uuid -> {
            this.subscribers.remove(uuid);
        });
        this.sources.remove(source);
    }

    public synchronized UUID subscribe(final Object source, final Observer<V> observer) {
        if (!this.sources.containsKey(source)) {
            this.sources.put(source, new ArrayList<UUID>());
        }
        final var subscriptions = this.sources.get(source);
        final var uuid = UUID.randomUUID();
        this.subscribers.put(uuid, observer);
        subscriptions.add(uuid);
        return uuid;
    }

    public void notify(final Event<V> event) throws GuardException {
        for (final var observer : this.subscribers.values()) {
            observer.onChange(event);
        }
    }

    public void notifyNoExcept(final Event<V> event) {
        this.subscribers.forEach((key, observer) -> {
            try {
                observer.onChange(event);
            } catch (final Throwable e) {
                logger.log(Level.WARNING, "ignored on notifyNoExcept cycle", e);
            }
        });
    }

    public void notifyAsynch(final Event<V> event) {
        this.subscribers.forEach((key, observer) -> {
            executor.execute(() -> {
                try {
                    observer.onChange(event);
                } catch (final Throwable e) {
                    logger.log(Level.WARNING, "ignored on notifyAsynch cycle", e);
                }
            });
        });
    }

}
