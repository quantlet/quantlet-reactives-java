//
//     QuantLET - an event driven framework for large scale real-time analytics
//
//     Copyright (C) 2006 Jorge M. Faleiro Jr.
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Affero General Public License as published
//     by the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Affero General Public License for more details.
//
//     You should have received a copy of the GNU Affero General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
package quantlet.reactive.local;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import quantlet.reactive.Callable;
import quantlet.reactive.Context;
import quantlet.reactive.F;
import quantlet.reactive.R;
import quantlet.reactive.Reactive;

public class LocalContext extends Context {
    private static final Executor executor = Executors.newFixedThreadPool(3);

    @Override
    public <T> R<T> r(T value) {
        return new RLocal<T>(executor, value);
    }

    @Override
    public <T> R<T> r(String name, T value) {
        return new RLocal<T>(executor, name, value);
    }

    @Override
    public <T> F<T> f(Callable<T> callable, Reactive<?>... args) {
        return new FLocal<T>(executor, callable, args);
    }

    
}
    

