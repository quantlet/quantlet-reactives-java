//
//     QuantLET - an event driven framework for large scale real-time analytics
//
//     Copyright (C) 2006 Jorge M. Faleiro Jr.
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Affero General Public License as published
//     by the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Affero General Public License for more details.
//
//     You should have received a copy of the GNU Affero General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
package quantlet.reactive.local;

import java.util.concurrent.Executor;
import java.util.logging.Level;
import java.util.logging.Logger;

import quantlet.reactive.Event;
import quantlet.reactive.GuardException;
import quantlet.reactive.R;

public class RLocal<V> extends ReactiveLocal<V> implements R<V> {

    private static final Logger logger = Logger.getLogger(RLocal.class.getName());

    private V value;

    public RLocal(final Executor executor, final V value) {
        this(executor, null, value);
    }

    public RLocal(final Executor executor, final String name) {
        this(executor, name, null);
    }

    public RLocal(final Executor executor) {
        this(executor, null, null);
    }

    public RLocal(final Executor executor, final String name, final V value) {
        super(executor, name);
        this.value = value;
    }

    public void set(final V value, final Object source) throws GuardException {
        final V before = this.value;
        this.value = value;
        final var event = new Event<V>(this, before, () -> value, source);
        try {
            super.pre().notify(event);
            try {
                super.post().notifyNoExcept(event);
                super.async().notifyAsynch(event);
            } catch (final Throwable e) {
                logger.log(Level.SEVERE, "exception on noExcept cycle", e);
            }
        } catch (final Throwable t) {
            this.value = before;
            throw t;
        }
    }

    @Override
    public V get() {
        return this.value;
    }

    @Override
    public void set(V value) throws GuardException {
        this.set(value, null);
    }

}
