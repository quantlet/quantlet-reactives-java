//
//     QuantLET - an event driven framework for large scale real-time analytics
//
//     Copyright (C) 2006 Jorge M. Faleiro Jr.
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Affero General Public License as published
//     by the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Affero General Public License for more details.
//
//     You should have received a copy of the GNU Affero General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
package quantlet.reactive.local;

import java.util.concurrent.Executor;

import quantlet.reactive.Callable;
import quantlet.reactive.Event;
import quantlet.reactive.F;
import quantlet.reactive.Reactive;

public class FLocal<V> extends ReactiveLocal<V> implements F<V> {
    private final Callable<V> callable;

    private V lastValue;

    private final Reactive<?>[] args;

    public FLocal(final Executor executor, final Callable<V> callable, final Reactive<?>... args) {
        this(executor, null, callable, args);
    }

    public FLocal(final Executor executor, final String name, final Callable<V> callable, final Reactive<?>... args) {
        super(executor, name);
        this.callable = callable;
        this.args = args;
        this.lastValue = callable.call();
        this.bind();
    }

    private Event<V> transformEvent(final Event<?> event) {
        return new Event<V>(event.target, this.lastValue, this.callable, event);
    }

    public void bind() {
        for (final var arg : args) {
            arg.pre().subscribe(this, (event) -> super.pre().notify(transformEvent(event)));
            arg.post().subscribe(this, (event) -> super.post().notifyNoExcept(transformEvent(event)));
            arg.async().subscribe(this, (event) -> {
                super.async().notifyAsynch(transformEvent(event));
                this.lastValue = this.get();
            });
        }
    }

    public void unbind() {
        for (final var arg : args) {
            arg.pre().unsubscribe(this);
            arg.post().unsubscribe(this);
            arg.async().unsubscribe(this);
        }

    }

    @Override
    public V get() {
        return this.callable.call();
    }

}
