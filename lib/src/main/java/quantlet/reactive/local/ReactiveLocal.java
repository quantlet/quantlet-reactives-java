//
//     QuantLET - an event driven framework for large scale real-time analytics
//
//     Copyright (C) 2006 Jorge M. Faleiro Jr.
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Affero General Public License as published
//     by the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Affero General Public License for more details.
//
//     You should have received a copy of the GNU Affero General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
package quantlet.reactive.local;

import java.util.UUID;
import java.util.concurrent.Executor;

import quantlet.reactive.Reactive;
import quantlet.reactive.Subscription;

public abstract class ReactiveLocal<V> implements Reactive<V> {
    private final Subscription<V> pre;
    private final Subscription<V> post;
    private final Subscription<V> notify;

    private final String name;

    public ReactiveLocal(final Executor executor, final String name) {
        this.name = name == null ? UUID.randomUUID().toString() : name;
        this.pre = new SubscriptionLocal<>(executor);
        this.post = new SubscriptionLocal<>(executor);
        this.notify = new SubscriptionLocal<>(executor);

    }

    public ReactiveLocal(final Executor executor) {
        this(executor, null);
    }

    public abstract V get();

    public String getName() {
        return name;
    }

    public Subscription<V> pre() {
        return pre;
    }

    public Subscription<V> post() {
        return post;
    }

    public Subscription<V> async() {
        return notify;
    }

}
