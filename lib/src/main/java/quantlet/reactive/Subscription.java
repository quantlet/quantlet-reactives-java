//
//     QuantLET - an event driven framework for large scale real-time analytics
//
//     Copyright (C) 2006 Jorge M. Faleiro Jr.
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Affero General Public License as published
//     by the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Affero General Public License for more details.
//
//     You should have received a copy of the GNU Affero General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
package quantlet.reactive;

import java.util.UUID;

public interface Subscription<V> {

    public void unsubscribe(final Object source);

    public UUID subscribe(final Object source, final Observer<V> observer);

    public void notify(final Event<V> event) throws GuardException;

    public void notifyNoExcept(final Event<V> event);

    public void notifyAsynch(final Event<V> event);

}
