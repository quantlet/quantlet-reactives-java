//
//     QuantLET - an event driven framework for large scale real-time analytics
//
//     Copyright (C) 2006 Jorge M. Faleiro Jr.
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Affero General Public License as published
//     by the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Affero General Public License for more details.
//
//     You should have received a copy of the GNU Affero General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
package quantlet.reactive;

public class Event<V> {

    public final Object target;
    public final V before;
    private final Callable<V> after;
    public final Object source;

    public Event(final Object target, final V before, final Callable<V> after, final Object source) {
        this.source = source;
        this.before = before;
        this.after = after;
        this.target = target;
    }

    public Event(Object target, V before, Callable<V> after) {
        this(target, before, after, null);
    }

    public V getAfter() {
        return after.call();
    }

    

}
