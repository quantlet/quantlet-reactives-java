//
//     QuantLET - an event driven framework for large scale real-time analytics
//
//     Copyright (C) 2006 Jorge M. Faleiro Jr.
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Affero General Public License as published
//     by the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Affero General Public License for more details.
//
//     You should have received a copy of the GNU Affero General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
package quantlet.reactive;

import java.net.URI;

import quantlet.reactive.local.LocalContext;

public abstract class Context {

    public static Context newContext(final URI uri) {
        if (uri == null) {
            return new LocalContext();
        } else {
            throw new RuntimeException(String.format("invalid argument: %s", uri));
        }
    }

    public static Context newContext() {
        return newContext(null);
    }

    public abstract <T> R<T> r(T value);
    public abstract <T> R<T> r(final String name, T value);
    public abstract <T> F<T> f(final Callable<T> callable, Reactive<?>... args);
    
}
