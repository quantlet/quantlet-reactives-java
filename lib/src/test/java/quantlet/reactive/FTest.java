//
//     QuantLET - an event driven framework for large scale real-time analytics
//
//     Copyright (C) 2006 Jorge M. Faleiro Jr.
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Affero General Public License as published
//     by the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Affero General Public License for more details.
//
//     You should have received a copy of the GNU Affero General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
package quantlet.reactive;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class FTest {
    @Test
    void testCreation() {
        final var ctx = Context.newContext();
        final var a = ctx.r(1);
        final var b = ctx.r(2);
        final var sum = ctx.f(() -> a.get() + b.get(), a, b);
        assertEquals(sum.get(), 3);
    }

    @Test
    void testReaction() throws GuardException {
        final var ctx = Context.newContext();
        final var a = ctx.r(1);
        final var b = ctx.r(2);
        final var sum = ctx.f(() -> a.get() + b.get(), a, b);
        assertEquals(sum.get(), 3);

        a.set(123);
        assertEquals(sum.get(), 125);

    }
}
