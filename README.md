# quantlet-java

Core QuantLET in `OpenJDK` 11. All batteries included for use in `Docker Deskto`, `VSCode`, and `Dev Containers`.

Before anything else make sure to read and understand the [licensing requirements](LICENSE) for use.

## Build

```bash
gradle build
```

## Test

```bash
gradle test
```
